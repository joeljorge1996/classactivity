import ballerina/http;
import ballerina/io;

# A service representing a network-accessible API
# bound to absolute path `/hello` and port `9090`.
# 
# 

type product record {
    int product_id;
    string name;
    float price;
    int qty;
};

product [] stockList = [];



service /eShop on new http:Listener(9090) {


   
 @http:ResourceConfig {
        consumes: ["application/json"]
    }

    resource function post addProduct(@http:Payload product new_item) returns json {

        io:println("Processing new product in stock ....");
        stockList.push(new_item);
        return {"Add":"Successfully added "+new_item.name+" to the shop stock list"};

    }


    resource function get getProducts() returns product[] {
        
        io:println("Producing the stock list to the client.");
        return stockList;
    }


     
    }

